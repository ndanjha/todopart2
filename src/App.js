import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";
import TodoList from "./TodoList"
import { Route, NavLink, BrowserRouter } from "react-router-dom";

class App extends Component {
  state = { 
    todos: todosList,
    value: ""
  };

  handleChange = event => {
    this.setState({ value: event.target.value}); 
  }

  handleCreateTodo = event => {
    if (event.key === "Enter"){
    const newTodo ={
      userId: 1,
      id: Math.floor(Math.random() * 10000),
      title: this.state.value,
      completed: false
    };
    const newTodos = this.state.todos.slice();
    newTodos.push(newTodo);
    this.setState({todos: newTodos, value: ''})
  }
  };
  handleDeleteTodo = todoIdToDelete => (event) => {
    //create a copy
    const newTodos = this.state.todos.slice();
// modify copy
    const todoIndexToDelete = newTodos.findIndex(todo => {
      if (todo.id === todoIdToDelete){
        return true;
      } else {
        return false;
      }
    })
    newTodos.splice(todoIndexToDelete, 1 )
  //overwrite original copy
    this.setState({todos: newTodos})
  }

  handleDeleteSelected = () => {
    const newTodos = this.state.todos.filter(todo => {
      if(todo.completed === true){
        return false
      } else{
        return true
      }
    })
    this.setState({todos: newTodos});
  }

  completedToggle = (todoIdToToggle) => (event) =>{
   const newTodo = this.state.todos.map(todo => {
     if(todo.id === todoIdToToggle){
     if(todo.completed === true){
       return todo.completed = false;
     } else {
       todo.completed = true;
     } }return todo
   });
   this.setState({todos: newTodo});
  }

  completedLineThrough = (todos) => {
    if(todos.completed === true){
      return todos.completed = true;
    }else{
      return todos.completed = false;
    }
  };


  render() {
    return (
      <BrowserRouter>
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autofocus
            value={this.state.value}
            onChange={this.handleChange}
            onKeyDown={this.handleCreateTodo}
          />
        </header>
        <Route 
        exact
        path="/active"
        render= {() => (
           <TodoList 
            todos={this.state.todos.filter(todo => todo.completed !== true)} 
            completedToggle={this.completedToggle}
            handleDeleteTodo={this.handleDeleteTodo}
    />  
    )} 
    />
      <Route 
      exact
        path="/"
        render= {() => (
           <TodoList 
            todos={this.state.todos} 
            completedToggle={this.completedToggle}
            handleDeleteTodo={this.handleDeleteTodo}
    />  
    )} 
    />
      <Route 
      exact
        path="/completed"
        render= {() => (
           <TodoList 
            todos={this.state.todos.filter(todo => todo.completed === true)} 
            completedToggle={this.completedToggle}
            handleDeleteTodo={this.handleDeleteTodo}
    />  
    )} 
    />
        <footer className="footer">
  {/* <!-- This should be `0 items left` by default --> */}
  <span className="todo-count">
    <strong>0</strong> item(s) left
  </span>
  <ul className="filters">
    <li>
      <NavLink exact to="/" activeClassName="selected">All</NavLink>
    </li>
    <li>
      <NavLink exact to="/active" activeClassName="selected">Active</NavLink>
    </li>
    <li>
      <NavLink exact to="/completed" activeClassName="selected">Completed</NavLink>
    </li>
  </ul>
  <button className="clear-completed" onClick={this.handleDeleteSelected}>Clear completed</button>
</footer>
      </section>
      </BrowserRouter>
    );
  }
}

export default App;
