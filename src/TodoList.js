import React, { Component } from "react";
import TodoItem from "./TodoItem"; 


class TodoList extends Component {
    render() {
      return (
        <section className="main">
          <ul className="todo-list">
            {this.props.todos.map(todo => (
              <TodoItem key={todo.id} handleDeleteTodo= {this.props.handleDeleteTodo(todo.id)
              }
               title={todo.title} 
               completedLineThrough={todo.completed}
               completedToggle={this.props.completedToggle(todo.id)}/>
            ))}
          </ul>
        </section>
      );
    }
  }
  
  export default TodoList;